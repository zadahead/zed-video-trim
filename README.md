# Getting Started with Zed-Trimmer

This project was made using 

- React 
- Typescript 
- Vanilla Css

no external modules have been used 

![Alt text](image.png)

## Installation

clone repo 

```bash
git clone https://gitlab.com/zadahead/zed-video-trim.git
```

install 

```bash
cd /zed-video-trim
npm install
```

run

```bash
npm start
```
